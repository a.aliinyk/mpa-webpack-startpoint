const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const merge = require("webpack-merge");
const pug = require("./webpack/pug");
const devServer = require("./webpack/devServer");
const sass = require("./webpack/sass");
const js = require("./webpack/js");
const img = require("./webpack/img");
const fonts = require("./webpack/fonts");
const css = require("./webpack/css");

// Add page names here
let { pagesNames } = require("./pages");

// function for generate pug - pages
function pagesPlugins(production) {
  return pagesNames.map(
    pageName =>
      new HtmlWebpackPlugin({
        filename: `${production ? pageName + "/" : ""}${pageName}.html`,
        chunks: ["commons", pageName, "styles"],
        inject: true,
        template: path.join(
          __dirname,
          `./src/pages/${pageName}/${pageName}.pug`
        )
      })
  );
}

function entrys() {
  const store = {};
  pagesNames.map(item => {
    store[item] = path.resolve(__dirname, `./src/pages/${item}/${item}.js`);
  });
  return store;
}

const common = merge([
  {
    optimization: {
      minimize: false,
      runtimeChunk: { name: "commons" },
      splitChunks: {
        cacheGroups: {
          styles: {
            name: "styles",
            test: /\.s?css$/,
            chunks: "all",
            enforce: true,
            minChunks: 1,
            reuseExistingChunk: true
          },
          commons: {
            test: /\.jsx?$/,
            chunks: "all",
            minChunks: 2,
            name: "commons",
            enforce: true
          }
        }
      }
    },
    output: {
      path: path.resolve(__dirname, "./dist")
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: "style/[name].css"
        // chunkFilename: "[id].css",
        // ignoreOrder: false
      }),

      {
        apply(compiler) {
          compiler.hooks.shouldEmit.tap(
            "Remove styles from output",
            compilation => {
              delete compilation.assets["styles/styles.js"];
              return true;
            }
          );
        }
      }
    ],
    resolve: {
      extensions: [".js", ".pug", ".scss", ".css"]
    }
  },
  pug(),
  fonts(),
  sass(),
  js(),
  img()
]);
module.exports = (env, options) => {
  let production = options.mode === "production";
  // check mode
  // pagesNames = production
  //   ? pagesNames.filter(item => item !== "index"):
  //     pagesNames;

  common.plugins = common.plugins.concat(pagesPlugins(production));

  common.output.filename = production ? "[name]/[name].js" : "[name].js";
  common.entry = entrys(production);

  if (production) {
    return merge([common]);
  } else {
    return merge([common, devServer]);
  }
};
