console.log("index");
import "../../scss/style.scss";
const { pagesNames } = require("../../../pages");
const rootEl = document.getElementById("root");

function createLink(linkName) {
  const el = document.createElement("a");
  el.setAttribute("href", `/${linkName}.html`);
  el.innerHTML = linkName;
  el.style.display = "block";
  rootEl.appendChild(el);
}
import "../../components/test";
window.onload = () => {
  console.log(pagesNames);

  pagesNames.forEach(item => {
    createLink(item);
  });
};
