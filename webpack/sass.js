const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const autoprefixer = require("autoprefixer");
module.exports = function(paths) {
  return {
    module: {
      rules: [
        {
          test: /\.(sa|sc|c)ss$/,
          include: paths,
          use: [
            {
              loader: MiniCssExtractPlugin.loader,
              options: {
                reloadAll: true,
                publicPath: "./styles/"
              }
            },
            {
              loader: "css-loader",
              options: { url: true }
            },
            {
              loader: "resolve-url-loader",
              options: { debug: true }
            },
            "sass-loader",
            {
              loader: "postcss-loader",
              options: {
                plugins: [
                  autoprefixer({
                    // browsers: ["ie >= 8", "last 4 version"]
                  })
                ],
                sourceMap: false
              }
            }
          ]
        }
      ]
    }
  };
};
