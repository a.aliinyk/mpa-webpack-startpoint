module.exports = function() {
  return {
    module: {
      rules: [
        {
          test: /\.(png|jpe?g|gif)$/i,
          loader: "file-loader",
          options: {
            outputPath: "assets/images",
            limit: 50
          }
        }
      ]
    }
  };
};
